package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;



import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.JList;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;

import java.awt.Label;
import java.awt.Color;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JComboBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import java.awt.Rectangle;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.border.LineBorder;

public class Principal extends JFrame {

	JPanel contentPane;
	JTextField textoIntentos;
	String palabras[] = rellenarPalabras();
	JList listaPalabras;
	String palabraAdivinar;
	JLabel[] barritas;
	int fallos = 0;
	int contadorVidas = 4;
	JTextField[] vidas;
	JButton btnQ;
	JButton btnW;
	JButton btnE;
	JButton btnR;
	JButton btnT;
	JButton btnY;
	JButton btnU;
	JButton btnI;
	JButton btnO;
	JButton btnP;
	JButton btnA;
	JButton btnS;
	JButton btnD;
	JButton btnF;
	JButton btnG;
	JButton btnH;
	JButton btnJ;
	JButton btnK;
	JButton btnL;
	JButton btnÑ;
	JButton btnZ;
	JButton btnX;
	JButton btnC;
	JButton btnV;
	JButton btnB;
	JButton btnN;
	JButton btnM;
	JLabel foto0;
	JLabel foto1;
	JLabel foto2;
	JLabel foto3;
	JLabel foto4;
	JLabel foto5;
	JTextField textoFallos;
	JButton botonNuevaPartida;
	JButton btnPista;
	JButton[] botones;
	JTextField vida1;
	JTextField vida2;
	JTextField vida3;
	JTextField vida4;
	JTextField vida5;
	JMenu acercaDe;
	JMenuItem ayuda;
	JMenuItem comoJugar;
	JMenuBar menuBar;


	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 881, 592);
		
		//MENU
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
				
		acercaDe = new JMenu("Acerca de");
		menuBar.add(acercaDe);
		
		ayuda = new JMenuItem("Ayuda");
		acercaDe.add(ayuda);
		ayuda.addActionListener(ayudaPanel);
		
		comoJugar = new JMenuItem("Como jugar");
		menuBar.add(comoJugar);
		comoJugar.addActionListener(comoJugarPanel);				
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);		
		
		//TECLADO
		btnQ = new JButton("Q");
		btnQ.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnQ.setBounds(110, 210, 41, 23);
		contentPane.add(btnQ);
		
		 btnW = new JButton("W");
		btnW.setFont(new Font("Tahoma", Font.PLAIN, 9));		
		btnW.setBounds(159, 210, 49, 23);
		contentPane.add(btnW);
		
		
		 btnE = new JButton("E");
		btnE.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnE.setBounds(218, 210, 39, 23);
		contentPane.add(btnE);
		
		 btnR = new JButton("R");
		btnR.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnR.setBounds(266, 210, 39, 23);
		contentPane.add(btnR);
		
		 btnT = new JButton("T");
		btnT.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnT.setBounds(315, 210, 39, 23);
		contentPane.add(btnT);
		
		 btnY = new JButton("Y");
		btnY.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnY.setBounds(364, 210, 39, 23);
		contentPane.add(btnY);
		
		 btnU = new JButton("U");
		btnU.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnU.setBounds(413, 210, 39, 23);
		contentPane.add(btnU);
		
		 btnI = new JButton("I");
		btnI.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnI.setBounds(466, 210, 39, 23);
		contentPane.add(btnI);
		
		 btnO = new JButton("O");
		btnO.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnO.setBounds(517, 210, 49, 23);
		contentPane.add(btnO);
		
		 btnP = new JButton("P");
		btnP.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnP.setBounds(576, 210, 39, 23);
		contentPane.add(btnP);
		
		 btnA = new JButton("A");
		btnA.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnA.setBounds(110, 244, 39, 23);
		contentPane.add(btnA);
		
		 btnS = new JButton("S");
		btnS.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnS.setBounds(159, 244, 39, 23);
		contentPane.add(btnS);
		
		 btnD = new JButton("D");
		btnD.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnD.setBounds(208, 244, 39, 23);
		contentPane.add(btnD);
		
		 btnF = new JButton("F");
		btnF.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnF.setBounds(256, 244, 39, 23);
		contentPane.add(btnF);
		
		btnG = new JButton("G");
		btnG.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnG.setBounds(305, 244, 39, 23);
		contentPane.add(btnG);
		
		btnH = new JButton("H");
		btnH.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnH.setBounds(364, 244, 39, 23);
		contentPane.add(btnH);
		
		btnJ = new JButton("J");
		btnJ.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnJ.setBounds(413, 244, 39, 23);
		contentPane.add(btnJ);
		
		btnK = new JButton("K");
		btnK.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnK.setBounds(466, 244, 39, 23);
		contentPane.add(btnK);
		
		btnL = new JButton("L");
		btnL.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnL.setBounds(517, 244, 39, 23);
		contentPane.add(btnL);
		
		btnÑ = new JButton("Ñ");
		btnÑ.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnÑ.setBounds(566, 244, 39, 23);
		contentPane.add(btnÑ);
		
		 btnZ = new JButton("Z");
		btnZ.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnZ.setBounds(188, 278, 39, 23);
		contentPane.add(btnZ);
		
		 btnX = new JButton("X");
		btnX.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnX.setBounds(237, 280, 39, 23);
		contentPane.add(btnX);
		
		 btnC = new JButton("C");
		btnC.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnC.setBounds(286, 280, 39, 23);
		contentPane.add(btnC);
		
	    btnV = new JButton("V");
		btnV.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnV.setBounds(334, 280, 39, 23);
		contentPane.add(btnV);
		
		btnB = new JButton("B");
		btnB.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnB.setBounds(383, 280, 39, 23);
		contentPane.add(btnB);
		
		
		btnN = new JButton("N");
		btnN.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnN.setBounds(432, 280, 39, 23);
		contentPane.add(btnN);
		
		btnM = new JButton("M");
		btnM.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnM.setBounds(481, 280, 49, 23);
		contentPane.add(btnM);		
		
		listaPalabras = rellenarLista(palabras);
		listaPalabras.setBounds(707, 273, 148, 226);
		contentPane.add(listaPalabras);

		botonNuevaPartida = new JButton("Nueva Partida");
		botonNuevaPartida.setBounds(237, 416, 117, 35);
		contentPane.add(botonNuevaPartida);
		
		//BOTON SALIR
		JButton salir = new JButton("Salir");		
		salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		salir.setBounds(383, 416, 117, 35);
		contentPane.add(salir);
		
		//AGREGAMOS FOTOS
		foto0 = new JLabel("");
		foto0.setIcon(new ImageIcon("..\\PalabraOculta\\src\\main\\java\\imagenes\\Captura.png"));
		foto0.setBounds(673, 23, 182, 244);
		contentPane.add(foto0);
		
		foto1 = new JLabel("");
		foto1.setIcon(new ImageIcon("..\\PalabraOculta\\src\\main\\java\\imagenes\\Captura1.png"));
		foto1.setBounds(673, 44, 207, 194);
		contentPane.add(foto1);
		
		foto2 = new JLabel("");
		foto2.setIcon(new ImageIcon("..\\PalabraOculta\\src\\main\\java\\imagenes\\Captura2.png"));
		foto2.setBounds(673, 44, 182, 194);
		contentPane.add(foto2);
		
		foto3 = new JLabel("");
		foto3.setIcon(new ImageIcon("..\\PalabraOculta\\src\\main\\java\\imagenes\\Captura3.png"));
		foto3.setBounds(673, 44, 182, 194);
		contentPane.add(foto3);
		
		foto4 = new JLabel("");
		foto4.setIcon(new ImageIcon("..\\PalabraOculta\\src\\main\\java\\imagenes\\Captura4.png"));
		foto4.setBounds(673, 44, 182, 194);
		contentPane.add(foto4);
		
		foto5 = new JLabel("");
		foto5.setIcon(new ImageIcon("..\\PalabraOculta\\src\\main\\java\\imagenes\\Captura5.png"));
		foto5.setBounds(673, 44, 182, 194);
		contentPane.add(foto5);
		
		textoFallos = new JTextField();
		textoFallos.setColumns(10);
		textoFallos.setBounds(112, 389, 86, 20);
		contentPane.add(textoFallos);		
		
		btnPista = new JButton("PISTA");
		btnPista.setBounds(314, 352, 89, 23);
		contentPane.add(btnPista);
		
		JLabel lblFallos = new JLabel("Fallos:");
		lblFallos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblFallos.setBounds(24, 385, 86, 23);
		contentPane.add(lblFallos);
				
		JLabel lblNewLabel_1 = new JLabel("VIDAS:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(208, 480, 84, 14);
		contentPane.add(lblNewLabel_1);
		
		//VIDAS
		vida1 = new JTextField();
		vida1.setBackground(Color.RED);
		vida1.setBounds(305, 479, 20, 20);
		contentPane.add(vida1);
		vida1.setColumns(10);
		
		vida2 = new JTextField();
		vida2.setColumns(10);
		vida2.setBackground(Color.RED);
		vida2.setBounds(334, 479, 20, 20);
		contentPane.add(vida2);
		
		vida3 = new JTextField();
		vida3.setColumns(10);
		vida3.setBackground(Color.RED);
		vida3.setBounds(362, 479, 20, 20);
		contentPane.add(vida3);
		
		vida4 = new JTextField();
		vida4.setColumns(10);
		vida4.setBackground(Color.RED);
		vida4.setBounds(391, 479, 20, 20);
		contentPane.add(vida4);
		
		vida5 = new JTextField();
		vida5.setColumns(10);
		vida5.setBackground(Color.RED);
		vida5.setBounds(421, 479, 20, 20);
		contentPane.add(vida5);

		
		//POR DEFECTO LAS FOTOS ESTARAN ESCONDIDAS
		foto0.setVisible(false);
		foto1.setVisible(false);
		foto2.setVisible(false);
		foto3.setVisible(false);
		foto4.setVisible(false);
		foto5.setVisible(false);

		//AGREGAMOS LOS EVENTOS
		botonNuevaPartida.addActionListener(nuevaPartida);
		btnQ.addActionListener(teclas);
		btnW.addActionListener(teclas);
		btnE.addActionListener(teclas);
		btnR.addActionListener(teclas);
		btnT.addActionListener(teclas);
		btnY.addActionListener(teclas);
		btnU.addActionListener(teclas);
		btnI.addActionListener(teclas);
		btnO.addActionListener(teclas);
		btnP.addActionListener(teclas);
		btnA.addActionListener(teclas);
		btnS.addActionListener(teclas);
		btnD.addActionListener(teclas);
		btnF.addActionListener(teclas);
		btnG.addActionListener(teclas);
		btnH.addActionListener(teclas);
		btnJ.addActionListener(teclas);
		btnK.addActionListener(teclas);
		btnL.addActionListener(teclas);
		btnÑ.addActionListener(teclas);
		btnZ.addActionListener(teclas);
		btnX.addActionListener(teclas);
		btnC.addActionListener(teclas);
		btnV.addActionListener(teclas);
		btnB.addActionListener(teclas);
		btnN.addActionListener(teclas);
		btnM.addActionListener(teclas);
		btnPista.addActionListener(pista);
		botones = llenarArrayBotones();//LLENAMOS ARRAY CON TODOS LOS BOTONES
		deshabilitarBotones();//BOTONES DESHABILITADOS POR DEFECTO
		vidas = llenarVidas();//RELLENAMOS LAS VIDAS AL MÁXIMO
		

	}
	
	
	
	
	//EVENTOS	
			ActionListener nuevaPartida = new ActionListener() {
				@Override
					public void actionPerformed(ActionEvent e) {						
						palabraAdivinar = palabraAleatoria();
						barritas = new JLabel[palabraAdivinar.length()];
						int contadorBarrita = 0;//Contador que va aumentando la posicion de cada barrita
						habilitarBotones();
						botonNuevaPartida.setEnabled(false);//Deshabilitamos el boton de nueva partida
						for (int i = 0; i < palabraAdivinar.length(); i++) {//Creamos labels dependiendo de la longitud de la palabra
							barritas[i]= new JLabel("");
							barritas[i].setFont(new Font("Tahoma", Font.PLAIN, 25));
							barritas[i].setBounds(240+contadorBarrita, 100, 86, 23);		
							contentPane.add(barritas[i]);
							barritas[i].setName("etiqueta"+i);
							barritas[i].setText("-");//Le decimos que el texto sea una barra							
							contadorBarrita += 30;//Aumentamos contador				
						}
										
					}
				};
				
				
				ActionListener teclas = new ActionListener() {					
					@Override
					public void actionPerformed(ActionEvent e) {					
						JButton teclaPulsada = (JButton) e.getSource(); //Detectamos que boton se ha seleccionado
						boolean fallado = true;//Detectamos si ha fallado
						
						//Recorremos la palabra
						for (int i = 0; i < palabraAdivinar.length(); i++) {
							String letra = Character.toString(palabraAdivinar.charAt(i));						
							if(teclaPulsada.getText().equals(letra)) { //Si la letra de la palabra es igual a la letra introducida
								barritas[i].setText(letra);//Cambiamos la barra por la letra
								teclaPulsada.setEnabled(false);//Deshabiltiamos la tecla
								fallado = false;							
							}							
						}
						
						if(fallado) {//Si fallamos sumamos el contador
							fallos++;
						}
						textoFallos.setText(Integer.toString(fallos));//Actualizamos contador
			
						//Dependiendo de los fallos va mostrando las diferentes fotos
						if(fallos >= 0 && fallos < 2) {
							foto0.setVisible(true);

						}else if(fallos >= 2 && fallos < 4) {
							foto0.setVisible(false);
							foto1.setVisible(true);

						}else if(fallos >= 4 && fallos < 6) {
							foto1.setVisible(false);

							foto2.setVisible(true);

						}else if(fallos >= 6 && fallos < 8) {
							foto2.setVisible(false);

							foto3.setVisible(true);

						}else if(fallos >=8 && fallos <10) {
							foto3.setVisible(false);

							foto4.setVisible(true);

						}else {
							//Si pierdes muestra la ultima foto, deshabilita los botones y reinicia el juego
							foto4.setVisible(false);
							foto5.setVisible(true);
							deshabilitarBotones();	
							resetGame();
							
						}
						
						//ACABAR
						String palabra = "";
						for (int j = 0; j < barritas.length; j++) {
							 palabra = palabra + barritas[j].getText();				
								if(palabra.equals(palabraAdivinar)) {
								j=barritas.length;
								deshabilitarBotones();	
								JOptionPane.showMessageDialog(null, "Dale a <Nueva Partida> para jugar una nueva partida");
								resetGame();																
								
							}
						}
	
						
					}
				};
				
				//Cuando pulse el boton pista
				ActionListener pista = new ActionListener() {	
					@Override
					public void actionPerformed(ActionEvent e) {
					vidas[contadorVidas].setVisible(false);
					contadorVidas--;//Le restamos una vida
						
					char[] arrayCadena = palabraAdivinar.toCharArray();
					String letraElegida = Character.toString(arrayCadena[0]);
					barritas[0].setText(letraElegida);//Le mostramos la primera letra de la palabra
					JOptionPane.showMessageDialog(null, "Se ha agregado la primera letra de la palabra");
					btnPista.setEnabled(false);//Deshabilitamos el boton pista
					
					for (int j = 0; j < botones.length; j++) {
						if(botones[j].getText().equals(letraElegida)) {
							botones[j].setEnabled(false);//Deshabilitamos el boton de la letra que nos da la pista
						}
					}
					}					
				};
				
				//Cuando pulse el boton de comoJugar
				ActionListener comoJugarPanel = new ActionListener() {										
					@Override
					public void actionPerformed(ActionEvent e) {
						JOptionPane.showMessageDialog(null, "Como jugar");					
					}					
				};				
				
				//Cuando pulse el boton ayuda
				ActionListener ayudaPanel = new ActionListener() {										
					@Override
					public void actionPerformed(ActionEvent e) {
						JOptionPane.showMessageDialog(null, "Daniel Jiménez Félix, Alejandro Largo, Juan José Carretero, Marc Infante");					
					}
					
				};
				
//Metodo que reinicia el juego	
public void resetGame() {
	fallos = 0;
	textoFallos.setText(Integer.toString(fallos));
	for (int i = 0; i < barritas.length; i++) {
		barritas[i].setText(" ");
	}

	botonNuevaPartida.setEnabled(true);
}


//Metodo que rellena el array con vidas
public JTextField[] llenarVidas() {
	JTextField[] vidas = new JTextField[5];
	vidas[0] = vida1;
	vidas[1] = vida2;
	vidas[2] = vida3;
	vidas[3] = vida4;
	vidas[4] = vida5;	
	return vidas;

}
		
//Deshabilitar botones del teclado y el de pista
public void deshabilitarBotones() {
	 btnPista.setEnabled(false);
	 btnQ.setEnabled(false);
	 btnW.setEnabled(false);
	 btnE.setEnabled(false);
	 btnR.setEnabled(false);
	 btnT.setEnabled(false);
	 btnY.setEnabled(false);
	 btnU.setEnabled(false);
	 btnI.setEnabled(false);
	 btnO.setEnabled(false);
	 btnP.setEnabled(false);
	 btnA.setEnabled(false);
	 btnS.setEnabled(false);
	 btnD.setEnabled(false);
	 btnF.setEnabled(false);
	 btnG.setEnabled(false);
	 btnH.setEnabled(false);
	 btnJ.setEnabled(false);
	 btnK.setEnabled(false);
	 btnL.setEnabled(false);
	 btnÑ.setEnabled(false);
	 btnZ.setEnabled(false);
	 btnX.setEnabled(false);
	 btnC.setEnabled(false);
	 btnV.setEnabled(false);
	 btnB.setEnabled(false);
	 btnN.setEnabled(false);
	 btnM.setEnabled(false);
}

//Habilitar botones de teclado y el de Pista si le quedan vidas
public void habilitarBotones() {
	if(contadorVidas == -1) {
		btnPista.setEnabled(false);
	}else {
		 btnPista.setEnabled(true);

	}
	 btnQ.setEnabled(true);
	 btnW.setEnabled(true);
	 btnE.setEnabled(true);
	 btnR.setEnabled(true);
	 btnT.setEnabled(true);
	 btnY.setEnabled(true);
	 btnU.setEnabled(true);
	 btnI.setEnabled(true);
	 btnO.setEnabled(true);
	 btnP.setEnabled(true);
	 btnA.setEnabled(true);
	 btnS.setEnabled(true);
	 btnD.setEnabled(true);
	 btnF.setEnabled(true);
	 btnG.setEnabled(true);
	 btnH.setEnabled(true);
	 btnJ.setEnabled(true);
	 btnK.setEnabled(true);
	 btnL.setEnabled(true);
	 btnÑ.setEnabled(true);
	 btnZ.setEnabled(true);
	 btnX.setEnabled(true);
	 btnC.setEnabled(true);
	 btnV.setEnabled(true);
	 btnB.setEnabled(true);
	 btnN.setEnabled(true);
	 btnM.setEnabled(true);
}

//Llenar array de botones
public JButton[] llenarArrayBotones() {
	JButton[] botones = new JButton[27];
	botones[0] = btnQ;
	botones[1] = btnW;
	botones[2] = btnE;
	botones[3] = btnR;
	botones[4] = btnT;
	botones[5] = btnY;
	botones[6] = btnU;
	botones[7] = btnI;
	botones[8] = btnO;
	botones[9] = btnP;
	botones[10] = btnA;
	botones[11] = btnS;
	botones[12] = btnD;
	botones[13] = btnF;
	botones[14] = btnG;
	botones[15] = btnH;
	botones[16] = btnJ;
	botones[17] = btnK;
	botones[18] = btnL;
	botones[19] = btnÑ;
	botones[20] = btnZ;
	botones[21] = btnX;
	botones[22] = btnC;
	botones[23] = btnV;
	botones[24] = btnB;
	botones[25] = btnN;
	botones[26] = btnM;
	
	return botones;
}

//Obtener palabra aleatoria de la lista
public String palabraAleatoria() {
		String palabra = palabras[aleatorio(0, 9)];
		return palabra;
	}

//Llenar lista de palabras
	public JList rellenarLista(String palabras[]) {
		JList listaPalabras = new JList();
		DefaultListModel modelo = new DefaultListModel();
		modelo.addElement(palabras[0]);
		modelo.addElement(palabras[1]);
		modelo.addElement(palabras[2]);
		modelo.addElement(palabras[3]);
		modelo.addElement(palabras[4]);
		modelo.addElement(palabras[5]);
		modelo.addElement(palabras[6]);
		modelo.addElement(palabras[7]);
		modelo.addElement(palabras[8]);
		modelo.addElement(palabras[9]);


		listaPalabras.setModel(modelo);
		
		return listaPalabras;
	}
	
	
//Rellenar array de palabras
	public String[] rellenarPalabras() {
	 String palabras[] = new String[10];
	 
	 palabras[0] = "CATASTROFE";
	 palabras[1] = "DENTISTA";
	 palabras[2] = "RIENDAS";
	 palabras[3] = "MOTO";
	 palabras[4] = "COLADOR";
	 palabras[5] = "FINLANDIA";
	 palabras[6] = "PEINADO";
	 palabras[7] = "BIENESTAR";
	 palabras[8] = "ESCRIBIR";
	 palabras[9] = "ARTISTA";
	 
	 return palabras;

	}
	
	//Generar numero aleatorio
	public int aleatorio(int min, int max) {
        int num = (int)(Math.random()*(max-min+1)+min);
        return num;
    }
	
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
